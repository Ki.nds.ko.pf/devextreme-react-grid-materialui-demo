import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "@devexpress/dx-react-grid-bootstrap4/dist/dx-react-grid-bootstrap4.css";

import { Navbar, Nav } from "react-bootstrap";
import {
  Grid,
  Table,
  TableHeaderRow,
  PagingPanel,
} from "@devexpress/dx-react-grid-bootstrap4";

import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { LinkContainer } from "react-router-bootstrap";

import {
  PagingState,
  IntegratedPaging,
  SortingState,
  IntegratedSorting,
} from "@devexpress/dx-react-grid";

const rows = [
  { id: 0, product: "DevExtreme", owner: "DevExpress" },
  { id: 1, product: "DevExtreme Reactive", owner: "DevExpress" },
  { id: 2, product: "DevExtreme Reactive-LFI", owner: "DevExpress-LFI" },
  { id: 3, product: "DevExtreme Reactive-LFI", owner: "DevExpress-LFI" },
  { id: 4, product: "DevExtreme Reactive-LFI", owner: "DevExpress-LFI" },
  { id: 5, product: "DevExtreme Reactive-LFI", owner: "DevExpress-LFI" },
  { id: 6, product: "DevExtreme Reactive-LFI", owner: "DevExpress-LFI" },
  { id: 7, product: "DevExtreme Reactive-LFI", owner: "DevExpress-LFI" },
  { id: 8, product: "DevExtreme Reactive-LFI", owner: "DevExpress-LFI" },
  { id: 9, product: "DevExtreme Reactive-LFI", owner: "DevExpress-LFI" },
  { id: 21, product: "DevExtreme Reactive-LFI", owner: "DevExpress-LFI" },
  { id: 22, product: "DevExtreme Reactive-LFI", owner: "DevExpress-LFI" },
  { id: 23, product: "DevExtreme Reactive-LFI", owner: "DevExpress-LFI" },
  { id: 24, product: "DevExtreme Reactive-LFI", owner: "DevExpress-LFI" },
  { id: 25, product: "DevExtreme Reactive-LFI", owner: "DevExpress-LFI" },
  { id: 26, product: "DevExtreme Reactive-LFI", owner: "DevExpress-LFI" },
  { id: 27, product: "DevExtreme Reactive-LFI", owner: "DevExpress-LFI" },
  { id: 28, product: "DevExtreme Reactive-LFI", owner: "DevExpress-LFI" },
  { id: 29, product: "DevExtreme Reactive-LFI", owner: "DevExpress-LFI" },
  { id: 12, product: "DevExtreme Reactive-LFI", owner: "DevExpress-LFI" },
  { id: 22, product: "DevExtreme Reactive-LFI", owner: "DevExpress-LFI" },
  { id: 32, product: "DevExtreme Reactive-LFI", owner: "DevExpress-LFI" },
  { id: 42, product: "DevExtreme Reactive-LFI", owner: "DevExpress-LFI" },
  { id: 52, product: "DevExtreme Reactive-LFI", owner: "DevExpress-LFI" },
  { id: 62, product: "DevExtreme Reactive-LFI", owner: "DevExpress-LFI" },
  { id: 72, product: "DevExtreme Reactive-LFI", owner: "DevExpress-LFI" },
  { id: 82, product: "DevExtreme Reactive-LFI", owner: "DevExpress-LFI" },
  { id: 92, product: "DevExtreme Reactive-LFI", owner: "DevExpress-LFI" },
  { id: 112, product: "DevExtreme Reactive-LFI", owner: "DevExpress-LFI" },
];

const columns = [
  { name: "id", title: "ID" },
  { name: "product", title: "Product" },
  { name: "owner", title: "Owner" },
];

const HomeView = () => {
  return <h1>Hello World!</h1>;
};

const GridView = () => {
  return (
    <Grid rows={rows} columns={columns}>
      <PagingState defaultCurrentPage={0} pageSize={10} />
      <SortingState defaultSorting={[{ columnName: "id", direction: "asc" }]} />
      <IntegratedPaging />
      <IntegratedSorting />
      <Table />
      <TableHeaderRow showSortingControls />
      <PagingPanel />
    </Grid>
  );
};

function App() {
  return (
    <Router>
      <div className="App">
        <Navbar bg="dark" variant="dark">
          <Navbar.Brand href="#home">DevExtreme React LFI</Navbar.Brand>
          <Nav className="mr-auto">
            <LinkContainer exact={true} to="/">
              <Nav.Link>Home</Nav.Link>
            </LinkContainer>
            <LinkContainer to="/grid">
              <Nav.Link>Grid</Nav.Link>
            </LinkContainer>
          </Nav>
        </Navbar>
        <Route exact path="/" component={HomeView} />
        <Route exact path="/grid" component={GridView} />
      </div>
    </Router>
  );
}

export default App;
